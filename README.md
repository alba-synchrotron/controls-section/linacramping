[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![Python 3.11](https://img.shields.io/badge/python-3.11-green.svg)](https://www.python.org/downloads/release/python-3110/)
[![License: GPL v3+](https://img.shields.io/badge/License-GPLv3+-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![black]( https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
![Version 0.9.1](https://img.shields.io/badge/version-0.9.1-blue.svg)

# LinacRamping Device Server
LinacRamping is a generic ramping device that will ramp tango attributes with certains constrains.

### Table of contents
- [Introduction](#introduction)
- [How to use this device](#how-to-use-this-device)
- [Device Properties](#device-properties)
- [Device Commands](#device-commands)
- [Configuration files](#configuration-files)
- [Related proyects](#related-proyects)
- [About](#about)

## Introduction
### Project Overview

The aim of this project is to enhance the capabilities of the ALBA Linac by introducing the ability to ramp certain Tango attributes. The primary objective is to reduce operational stress on specific components and perform conditioning when necessary. This proactive approach aims to prevent vacuum peaks in acceleration section cavities, arcs in waveguides, and other potential issues.

### Device Functionality

The proposed device will systematically ramp attributes from a safe starting point to the expected working/conditioning point. This process is crucial for maintaining optimal performance and longevity of the components involved. In the event of a failure or reaching an alarm point, the ramp will be halted, and an email notification will be sent to predefined recipients.

### Ramp Configuration

Users can define a ramp by specifying the following parameters:

- Attribute to Ramp
- Readback Attribute (if it exists; use _None_ if not applicable)
- Start Point
- End Point
- Delta Setpoint
- Time Between Points

Additionally, each ramp is launched as an independent thread, allowing for the concurrent execution of multiple ramps. This design ensures flexibility and efficiency in managing the ramping process.

## How to use this device
### Ramp Execution

You have two options for performing a ramp:

1. Using a Configuration File:
   - Utilize the [config.yaml](#configyaml) file.
   - Execute the ramp using the [Ramp Attribute](#ramp-attribute) command.

2. Manual Ramp Execution:
   - Execute a manual ramp using the [Ramp Attribute Manual](#ramp-attribute-manual) command.

### Alarm Configuration

To enhance control and safety, the system allows you to configure related ramp alarms using an [alarms.yaml](#alarmsyaml) file. The device will subscribe to Tango CHANGE_EVENTs of attributes defined in this file. If an event value triggers an alarm, the ramp will be promptly stopped, and the last setpoint will be maintained.

This configuration provides an added layer of security and ensures that any unexpected changes in attribute values are promptly addressed during the ramping process.

## Device properties
Device properties are:
- **_alarm_file_** (_PATH_): Path to YAML file that contains alarm configuration. More info at [alarms.yaml](#alarmsyaml) section. If not defined, file path will be the same of the device code.
- **_config_file_** (_PATH_): Path to YAML file that contains ramp configurations. More info at [config.yaml](#configyaml) section. If not defined, file path will be the same of the device code.
- **_ky_mode_**(_STRING_): Property that indicates if we will ramp a klystron. If set to True, Linac KY will be set to ON state.
    - Default value: _True_.
- **_ramp_attributes_** (_STRING_)(_EXPERIMENTAL_): Property to define attributes to ramp not included in config.yaml file.
- **_alarms_** (_STRING_)(_EXPERIMENTAL_):Property to define aalarms not included in alarms.yaml file.

## Device Commands
We have 2 device commands:
- Ramp Attribute
- Ramp Attribute Manual
### Ramp Attribute
Command that ramp attributes defines in _config.yaml_ file.
- Input --> Attribute name defined in _config.yaml_ file as a python string.
    - Example: "sys/tg_test/1/ampli"
- Output --> None.
### Ramp Attribute Manual
Command that ramp an attribute.
- Input --> Ramp attribute from string _"attribute,readback,start,stop,delta_time,delta_value"_. You can find description below:
    - _Attribute_: Attribute to ramp in a/b/c format.
    - _Readback_: Readback attribute in a/b/c format. None if not needed.
    - _Start_: First point of the ramp.
    - _Stop_: last point of the ramp.
    - _Delta_time_: Time between ramp steps.
    - _Delta_value_: Step of the ramp.
    - Example: "sys/tg_test/1/ampli,None,0,10,1,1"
- Output: None.

### Klystron reset feature
To be documented.

## Configuration files
As commented before, there are 2 possible configuration files:
- config.yaml
- alarms.yaml
### config.yaml
File that contains attribute/s ramp configuration/s. You can see below an example:
```
sys/tg_test/1/ampli:  # Attribute to ramp
  readback: None  # Readback attribute not needed.
  start: 0.0 # Starting point.
  stop: 200.0 # Last point of the ramp.
  sleep_time: 0.5 # Time between applying setpoints.
  increase: 10 # Step.
sys/tg_test/1/double_scalar:
  readback: None
  start: 0.0
  stop: 40.0
  sleep_time: 5
  increase: 10
```
There is a widget tool to help the user to fill this file. Go to [Related proyects](#related-proyects) to see LinacRampWidgets.
### alarms.yaml
File that contains alarms configuration for each attribute that will ramp. You can see below an example:
```
li/ct/plc4/hvps_v_setpoint:  # Attribute affected by this alarm definition
  li/ct/plc4/pulse_st:  # Attribute to monitor
    min_value: 7  # Min.value that triggers alarm(included)
    max_value: 9  # Max. value that triggers the alarm.
  li/ct/plc2/hvg4_p:
    min_value: 1.0e-11
    max_value: 2.0e-8
li/ct/plc5/hvps_v_setpoint:  # Next attribute alarm configuration...
  li/ct/plc5/pulse_st:
    min_value: 7
    max_value: 9
  li/ct/plc2/hvg5_p:
    min_value: 1e-11
    max_value: 2e-8
```
There is a widget tool to help users to fill this file. Go to [Related proyects](#related-proyects) to see LinacRampWidgets.

## Related projects
Related to this project you can find a library of widgets to interct with this device server. [Here](https://gitlab.com/linacdevelopments/linacrampingwidgets) you can find the repository.

## About:
- Author: E.Morales (emorales@cells.es)
- Copyright: Copyright 2023, CELLS / ALBA Synchrotron

## Pendig
[ ] Document methods in code.