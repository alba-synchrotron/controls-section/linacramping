# -*- coding:utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Emilio Morales <emorales@cells.es>"
__maintainer__ = "Emilio Morales"
__copyright__ = "Copyright 2023, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


import os
import sys

import time
import yaml
from datetime import datetime
from math import ceil
from threading import Thread, Event
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

from tango import DevState, Except, AttributeProxy, EventType
from tango.server import Device, command, server_run, attribute
from tango.server import device_property

from .KlystronReset import KlystronReset


# Predefined e-mail header messages.
SETPOINT_ERROR = (
    "***Mail generated automatically***\n\n\n"
    "Error during LinacRamping. We can not apply last"
    "setpoint to KY. Details:\n"
)
KYON_ERROR = (
    "***Mail generated automatically***\n\n\n"
    "Error during LinacRamping. We can not set KY ON. "
    "Details:\n"
)
ALARM_ERROR = (
    "***Mail generated automatically***\n\n\n"
    "Error during LinacRamping. Alarm reached during "
    "ramping procedure. Details:\n"
)
KY_RESET_OK = (
    "***Mail generated automatically***\n\n\n"
    "Error during LinacRamping. Klystron reset automatically. "
)
KY_RESET_NOK = (
    "***Mail generated automatically***\n\n\n"
    "Error during LinacRamping. Klystron reset aborted. "
)

# Linac klystrons devices.
KY1_DEVICE = "li/ct/plc4"
KY2_DEVICE = "li/ct/plc5"

# Linac Config Keys
KY1_KEY = "li/ct/plc4/hvps_v_setpoint"
KY2_KEY = "li/ct/plc5/hvps_v_setpoint"


class LinacCallback(object):
    """Simple Callback method that return last event value from a Tango
    attribute.
    """

    _attr_value = None

    def __init__(self):
        super(object, self).__init__()

    def push_event(self, evt):
        """Method that save event value to class variable _attr_value

        :param evt: Tango event data received.
        :type evt: tango.EventData
        """
        self._attr_value = evt.attr_value.value

    @property
    def attr_value(self):
        """Property that returns last value stored in _attr_value

        :return: Attribute value
        :rtype: float
        """
        return self._attr_value


class LinacRamping(Device):
    config_file = device_property(dtype="string", default_value=None)
    ramp_attributes = device_property(
        dtype="DevVarStringArray", default_value=None
    )
    alarms = device_property(dtype="DevVarStringArray", default_value=None)
    alarm_file = device_property(dtype="string", default_value=None)
    ky_mode = device_property(dtype="string", default_value="True")
    ky_off = device_property(dtype="string", default_value="False")
    KyResetMode = device_property(dtype="string", default_value=False)

    _attribute_alarms = {}
    _attr_config = {}
    _attr_subscriptions = {}
    _ramping_threads = {}

    _ramping_attrs = []

    _attr = None

    _attr_cb = None

    _myList = []
    _myDict = {}

    _resetKy1 = None
    _resetKy2 = None

    _rampKy1 = None
    _rampKy2 = None

    _timeExpectedKy1 = 0
    _timeExpectedKy2 = 0

    _step_back = True

    def init_device(self):
        Device.init_device(self)
        self.info_stream("In init_device method")

        self._check_config_file()
        self._check_ramp_attributes()
        self._get_alarms_from_property()
        self._get_alarms_from_file()
        self._subscribe_alarms()
        self._get_reset_mode()

        if self._attr_config is not None:
            self.info_stream("Init complete")
            self.set_state(DevState.ON)
            self.set_status(
                "Device in ON State.\nAttributes to ramp:{}".format(
                    list(self._attr_config.keys())
                )
            )
        else:
            self.error_stream("No attributes to ramp.")
            self.set_state(DevState.FAULT)
            self.set_status("Device in fault. No attributes to ramp.")

    @attribute(label="RampKY1", dtype=bool, memorized=True, hw_memorized=True)
    def RampKY1(self):
        """Attribute to allow KY1 ramping mode.

        :return: Attribute value
        :rtype: bool
        """
        self.info_stream("In read RampKY1")
        return self._rampKy1

    @RampKY1.write
    def RampKY1(self, state):
        self.info_stream("In write RampKY1")
        self._rampKy1 = bool(state)

    @attribute(label="RampKY2", dtype=bool, memorized=True, hw_memorized=True)
    def RampKY2(self):
        """Attribute to allow KY2 ramping mode.

        :return: Attrtibute value
        :rtype: bool
        """
        self.info_stream("In read RampKY2")
        return self._rampKy2

    @RampKY2.write
    def RampKY2(self, state):
        self.info_stream("In write RampKY2")
        self._rampKy2 = bool(state)

    @attribute(label="KY1RampTime", dtype=float)
    def KY1RampTime(self):
        self._timeExpectedKy1 = self._get_ramp_time(KY1_KEY)

        return self._timeExpectedKy1

    @attribute(label="KY2RampTime", dtype=float)
    def KY2RampTime(self):
        self._timeExpectedKy2 = self._get_ramp_time(KY2_KEY)

        return self._timeExpectedKy2

    @attribute(label="KY1RampStart", dtype=float)
    def KY1RampStart(self):
        return float(self._attr_config[KY1_KEY]["start"])

    @attribute(label="KY2RampStart", dtype=float)
    def KY2RampStart(self):
        return float(self._attr_config[KY2_KEY]["start"])

    @attribute(label="KY1RampEnd", dtype=float)
    def KY1RampEnd(self):
        return float(self._attr_config[KY1_KEY]["stop"])

    @attribute(label="KY2RampEnd", dtype=float)
    def KY2RampEnd(self):
        return float(self._attr_config[KY2_KEY]["stop"])

    @attribute(label="KY1DeltaStep", dtype=float)
    def KY1DeltaStep(self):
        return float(self._attr_config[KY1_KEY]["increase"])

    @attribute(label="KY2DeltaStep", dtype=float)
    def KY2DeltaStep(self):
        return float(self._attr_config[KY2_KEY]["increase"])

    @attribute(label="KY1DeathTime", dtype=float)
    def KY1DeathTime(self):
        return float(self._attr_config[KY1_KEY]["sleep_time"])

    @attribute(label="KY2DeathTime", dtype=float)
    def KY2DeathTime(self):
        return float(self._attr_config[KY2_KEY]["sleep_time"])

    def _get_ramp_time(self, attribute):
        """Method to provide expected total time value of a ramp.
        Depends on config.yaml._summary_

        :param attribute: Name of attribute used as a config.yaml key.
        :type attribute: str
        :return: Return total ramp time.
        :rtype: float
        """
        try:
            (
                start_value,
                stop_value,
                increase,
                sleep_time,
            ) = self._config_my_ramp(attribute, return_proxy=False)
            time_expected = (
                (stop_value - start_value) / increase
            ) * sleep_time

            return float("{:.3f}".format(time_expected))

        except Exception as e:
            Except.throw_exception(
                "ValueError",
                "Problems in _get_ramp_time: {}".format(e),
                "LinacRamping",
            )

    def _get_reset_mode(self):
        """Method that creates necessary objects to provide a KY auto reset."""
        self.info_stream("In _get_reset_mode method")
        try:
            if self.KyResetMode in ["True"]:
                if self._resetKy1 is None and self._resetKy2 is None:
                    self.debug_stream("Configuring Klystron reset...")
                    self._resetKy1 = KlystronReset(KY1_DEVICE, datetime.now())
                    self._resetKy2 = KlystronReset(KY2_DEVICE, datetime.now())
                    self.debug_stream("Configuring Klystron reset...")
                else:
                    self.debug_stream(
                        "At least KlystronReset object defined. Aborting."
                    )
            else:
                self.debug_stream("KyResetMode set to False. Aborting.")
        except Exception as e:
            Except.throw_exception(
                "ValueError",
                "Problems in _get_reset_mode: {}".format(e),
                "LinacRamping",
            )

    def _subscribe_alarms(self):
        """Method that subscribe to attributes contained in alarms.yaml file"""
        for item in self._attribute_alarms:
            for attr in self._attribute_alarms[item]:
                _attr_cb = LinacCallback()
                my_proxy = AttributeProxy(attr)
                self._myDict[attr] = [my_proxy]

                try:
                    my_proxy.subscribe_event(EventType.CHANGE_EVENT, _attr_cb)
                    self._myDict[attr].append(_attr_cb)
                    self.info_stream("Subscription to {} done.".format(attr))

                except Exception as e:
                    Except.throw_exception(
                        "RuntimeError",
                        "Problerms in subscription: {}".format(e),
                        "LinacRamping",
                    )

    def _get_alarms_from_property(self):
        """Method to read and parse property 'alarms' and store in a dict"""
        if self.alarms is not None:
            for item in self.alarms:
                alarms = item.split(",")
                attr_ramp, attr_name, min_value, max_value = alarms
                self._attribute_alarms[attr_ramp] = {
                    attr_name: {
                        "attr_name": attr_name,
                        "min_value": min_value,
                        "max_value": max_value,
                    }
                }
            self.info_stream(
                "Alarms: {}".format(self._attribute_alarms.keys())
            )
        else:
            self.debug_stream("No alarm property defined.")

    def _get_alarms_from_file(self):
        """Method to get alarms from alarms.yaml file."""
        if self.alarm_file is not None:
            self.info_stream("Parsing alarms file")
            self._attribute_alarms = self._get_file_config(self.alarm_file)
        else:
            self.debug_stream("Alarm file is not present")

    def _check_config_file(self):
        """Method that check if config_file property and config.yaml file
        exists.
        """
        if self.config_file is not None:
            check = os.path.exists(self.config_file)
            if check is True:
                self.info_stream("Config. file exist.")
                self._attr_config = self._get_file_config(self.config_file)
            else:
                self.error_stream("Config. file does not exist. Path?")
        else:
            self.error_stream("config_file property is empty.")

    def _check_ramp_attributes(self):
        """Method to parse ramp_attributes property if it is not empty."""
        if self.ramp_attributes is None:
            self.error_stream("ramp_attributes property is empty.")
            return

        self.info_stream("Parsing ramp_attributes_property")

        for item in self.ramp_attributes:
            attr_data = item.split(",")
            if len(attr_data) == 6:
                (
                    attr_name,
                    readback,
                    start,
                    stop,
                    sleep_time,
                    increase,
                ) = attr_data
                if attr_name not in self._attr_config:
                    self._attr_config[attr_name] = {
                        "readback": readback,
                        "start": start,
                        "stop": stop,
                        "sleep_time": sleep_time,
                        "increase": increase,
                    }
                else:
                    self.error_stream(
                        "Attribute {} duplicated in config file.".format(
                            attr_name
                        )
                    )
            else:
                self.error_stream(
                    "Attribute {} have wrong configuration.".format(attr_name)
                )

        self.info_stream("Property parsed.")

    def _get_file_config(self, config_file):
        """Method that obtain configuration form config.yaml file.

        :param config_file: File path to config.yaml
        :type config_file: str
        :return: configuraiton dict.
        :rtype: dict
        """
        try:
            with open(config_file, "r") as f:
                conf = yaml.safe_load(f)
            f.close()
            return conf
        except Exception as e:
            Except.throw_exception(
                "ValueError",
                "Problems in _get_file_config: {}".format(e),
                "LinacRamping",
            )
            return None

    def _set_KY_on(self, attr_name, on=True):
        """Method to set Klystron ON or OFF.

        :param attr_name: Tango attribute name that contains KY info.
        :type attr_name: str
        :param on:  Parameter to decide if put klystron ON or OFF,
        defaults to True
        :type on: bool, optional
        """
        _on_attr = None
        if "plc4" in attr_name.lower():
            _on_attr = "li/ct/plc4/hvps_onc"
        if "plc5" in attr_name.lower():
            _on_attr = "li/ct/plc5/hvps_onc"

        try:
            if _on_attr is not None:
                attr_proxy = AttributeProxy(_on_attr)
                if on is True:
                    attr_proxy.write(True)
                else:
                    attr_proxy.write(False)
            else:
                self.error_stream(
                    "Problmes in _set_KY_on.Please check parameters."
                )
                self._send_mail("KY_ON", attr_name=attr_name.lower())
        except Exception as e:
            self.error_stream("Problmes in _set_KY_on: {}".format(e))
            self._send_mail("KY_ON", attr_name=attr_name.lower())
            Except.throw_python_exception(sys.exc_info())

    def _reset_ky(self, attr_name, ky_state=None):
        """Method to perform KY autoreset

        :param attr_name: Attribute actually ramping related withb KY that we
        want to reset.
        :type attr_name: str
        :param ky_state: Reason why KY is in interlock state, defaults to None.
        :type ky_state: int, optional
        :return: False if reset is performed, True otherwise.
        :rtype: bool
        """
        if "plc4" in attr_name:
            rst_obj = self._resetKy1
        elif "plc5" in attr_name:
            rst_obj = self._resetKy2

        try:
            reset = rst_obj.ResetKY()
            time.sleep(3)
            if reset is True:
                self._set_KY_on(attr_name)
                self._send_mail("RESET_OK")
                return False
            else:
                return True

        except Exception as e:
            self.error_stream("We can not perform KY reset. Check.")
            self.error_stream("Error: {}".format(e))
            self._send_mail("RESET_NOK")

    def _check_attribute_alarms(self, attr_name, stop_event, first_time=False):
        """Method that check if an alarm attribute is reaching limits defined.

        :param attr_name: Attribute to check
        :type attr_name: str
        :param stop_event: Control event to stop ramping attribute.
        :type stop_event: threading.Event
        :return: True when alarm is triggered
        :rtype: bool
        """
        attr_name = attr_name.lower()
        alarms_dict = self._attribute_alarms[attr_name]

        for item in alarms_dict:
            last_value = float(self._myDict[item][1].attr_value)
            min_value = float(alarms_dict[item]["min_value"])
            max_value = float(alarms_dict[item]["max_value"])
            self.info_stream(
                "\nLast value: {}\nMin alarm: {}\nMax alarm: {}".format(
                    last_value, min_value, max_value
                )
            )

            if last_value >= max_value or last_value <= min_value:
                if "hvg" in item:
                    self._set_KY_on(attr_name, on=False)

                if "pulse_st" in item and first_time is False:
                    if self.KyResetMode in ["True"]:
                        rst_status = self._reset_ky(item, stop_event)
                        if rst_status is True:
                            self._set_KY_on(attr_name, on=False)
                            stop_event.set()
                            self.error_stream(
                                "We can not perform a KY reset: {}".format(
                                    item
                                )
                            )
                            self._send_mail("RESET_NOK")

                            return True
                        else:
                            return False

                self.error_stream("We have an ALARM: {}".format(item))
                self._send_mail("ALARM", attr_name=attr_name)
                stop_event.set()
                return True

        return False

    def _check_attribute_limits(self, attr_proxy, value):
        """Method to check if value to be writtern in a Tango attribute is
        inside defined limits.

        :param attr_proxy: AttributeProxy object.
        :type attr_proxy: tango.AttributeProxy
        :param value: Value to check if is inside limits.
        :type value: str
        """
        attr_name = "{}/{}".format(
            attr_proxy.get_device_proxy().name(), attr_proxy.name()
        )

        max_value = float(attr_proxy.get_config().max_value)
        min_value = float(attr_proxy.get_config().min_value)

        if float(value) > max_value or float(value) < min_value:
            self._ramping_attrs.remove(attr_name)
            Except.throw_exception(
                "ValueError",
                "Setting is above or beyond limits",
                "LinacRamping",
            )

    def _get_actual_value(self, attr_proxy, readback_proxy):
        """Method that reads attribute._summary_

        :param attr_proxy: AttributeProxy object.
        :type attr_proxy: tango.AttributeProxy
        :param readback_proxy: Readback AttributeProxy object
        :type readback_proxy: tango.AttributeProxy
        :return: Actual value
        :rtype: float
        """

        if readback_proxy is not None:
            actual_value = readback_proxy.read().value
        else:
            actual_value = attr_proxy.read().value

        return actual_value

    def _go_to_starting_point(
        self,
        attr_proxy,
        attr_name,
        starting_point,
        stop_event,
        readback_proxy=None,
    ):
        """Method to go to start point if attribute is not in this value.

        :param attr_proxy: AttributeProxy object.
        :type attr_proxy: tango.AttributeProxy
        :param attr_name: Used to check alarms if necessary.
        :type attr_name: str
        :param starting_point: Value to write if necessary.
        :type starting_point: float
        :param stop_event: In case of problems (alarm), event to stop ramp.
        :type stop_event: threading.Event
        :param readback_proxy: Readback AttributeProxy object, defaults to None
        :type readback_proxy: tango.AttributeProxy, optional
        :return: True if attribute is in start point, False otherwise.
        :rtype: bool
        """
        if self.ky_mode == "False":
            is_alarm_active = self._check_attribute_alarms(
                attr_name, stop_event, first_time=True
            )
        else:
            is_alarm_active = False

        actual_value = self._get_actual_value(attr_proxy, readback_proxy)

        if is_alarm_active is False and float(starting_point) != float(
            actual_value
        ):
            attr_proxy.write(float(starting_point))

            time.sleep(0.3)

            for n in range(3):
                actual_value = self._get_actual_value(
                    attr_proxy, readback_proxy
                )

                if actual_value != float(starting_point):
                    time.sleep(0.2)
                else:
                    return True

            if actual_value != float(starting_point):
                return False
            else:
                return True
        else:
            return False

    def _check_value(self, attr_proxy, attr_name, value_applied, stop_event):
        """Method to check if value aplied is equal to readback value is inside
        a window.

        :param attr_proxy: Readback AttributeProxy object
        :type attr_proxy: tango.AttributeProxy
        :param attr_name: Name of readback attribute.
        :type attr_name: str
        :param value_applied: Last value applied to the ramp.
        :type value_applied: float
        :param stop_event: Stop event to set in case of failure.
        :type stop_event: threading.Event
        """
        val = attr_proxy.read().value

        if val > (value_applied - 0.05) or val < (value_applied + 0.05):
            self.info_stream(
                "Value {} applied. Continue.".format(value_applied)
            )
        else:
            self.error_stream(
                "Value {} NOT applied. Stopping.".format(value_applied)
            )
            self._send_mail("SETPOPINT", attr_proxy, attr_name, value_applied)
            stop_event.set()

    def _check_final_value(
        self, attr_proxy, actual_value, final_value, attr_name
    ):
        """Method to check if we reached final value of the ramp.

        :param attr_proxy: Proxy object of readback attribute.
        :type attr_proxy: tango.AttributeProxy
        :param actual_value: Actual vlaue of the Tango attribute.
        :type actual_value: float
        :param final_value: Final ramp value.
        :type final_value: float
        :param attr_name: Name of readback attribute.
        :type attr_name: str
        """
        attr_name = attr_name.lower()
        val = attr_proxy.read().value
        if val < (final_value + 0.05) or val > (final_value - 0.05):
            self.info_stream("Attribute {} ramped.".format(attr_name))
            self._ramping_attrs.remove(attr_name)
            del self._ramping_threads[attr_name]
        else:
            self.error_stream("Error ramping attribute {}".format(attr_name))
            if attr_name in self._ramping_attrs:
                self._ramping_attrs.remove(attr_name)
            if attr_name in self._ramping_threads:
                del self._ramping_threads[attr_name]
            self._send_mail("SETPOINT", attr_proxy, attr_name, actual_value)

    def _ramp_me(
        self,
        attr_proxy,
        readback,
        start_value,
        final_value,
        stop_event,
        inc,
        time_sleep=None,
    ):
        """Method that perform the attribute ramp.

        :param attr_proxy: AttributeProxy object of ramp attribute.
        :type attr_proxy: tango.AttributeProxy
        :param readback: Name of readback attribute.
        :type readback: str
        :param start_value: Start ramp value.
        :type start_value: float
        :param final_value: Final ramp expected value.
        :type final_value: float
        :param stop_event: Stop event to set in case of failure.
        :type stop_event: threading.Event
        :param inc: Increase of ramp. Delta setpoint.
        :type inc: float
        :param time_sleep: Time between ramp points., defaults to None
        :type time_sleep: float, optional
        """
        readback_proxy = None

        attr_name = "{}/{}".format(
            attr_proxy.get_device_proxy().name(), attr_proxy.name()
        )

        if readback.lower() != attr_name.lower():
            readback_proxy = AttributeProxy(readback)

        self.info_stream(
            "In _ramp_me method for attribute {}".format(attr_name)
        )

        step_back = True

        while step_back is True:
            step_back = False

            if stop_event.is_set() is True:
                break

            self._go_to_starting_point(
                attr_proxy, attr_name, start_value, stop_event, readback_proxy
            )
            actual_value = self._get_actual_value(attr_proxy, readback_proxy)

            step = ceil(
                abs(
                    float(
                        (float(final_value) - float(start_value)) / float(inc)
                    )
                    if float(inc) != 0.0
                    else 0.0
                )
            )

            if start_value > final_value:
                inc = inc * -1

            if self.ky_mode == "True":
                self._set_KY_on(attr_name)
                time.sleep(0.3)

            for n in range(step):
                step_back = self._check_attribute_alarms(attr_name, stop_event)
                self.info_stream("Step {} of {}".format(n, step))
                print("Stop Event: {}".format(stop_event.is_set()))
                if stop_event.is_set() is True:
                    self.info_stream("Stopping attribute {}".format(attr_name))
                    break

                actual_value = min(
                    max(
                        float(actual_value) + float(inc),
                        min(float(start_value), float(final_value)),
                    ),
                    max(float(start_value), float(final_value)),
                )

                if (
                    n > 0
                    and step_back is True
                    and self.KyResetMode in ["True"]
                ):
                    start_value = actual_value - (2 * inc)
                    break

                attr_proxy.write(actual_value)
                time.sleep(float(time_sleep) or 1)
                if readback_proxy is not None:
                    print("Readback proxy: {}".format(readback_proxy))
                    self._check_value(
                        readback_proxy, attr_name, actual_value, stop_event
                    )
                else:
                    print("No tenemos readback proxy")
                    self._check_value(
                        attr_proxy, attr_name, actual_value, stop_event
                    )

        if readback_proxy is not None:
            self._check_final_value(
                readback_proxy, actual_value, float(final_value), attr_name
            )
        else:
            self._check_final_value(
                attr_proxy, actual_value, float(final_value), attr_name
            )
        if self.ky_mode in ["True"] and self.ky_off in ["True"]:
            # Set KY OFF.
            self._set_KY_on(attr_name, on=False)

    def _config_my_ramp(self, attribute, return_proxy=True):
        """Method that return ramp configuration. If return_proxy=False 2 first
        attributes are not returned.

        :param attribute: Attribute name.
        :type attribute: str
        :param return_proxy: Readback attribute name, defaults to True
        :type return_proxy: bool, optional
        :return: attr, readback, start_value, stop_value, increase, sleep_time
        :rtype: str, str, float, float, float, float
        """
        readback = str(self._attr_config[attribute]["readback"])
        start_value = float(self._attr_config[attribute]["start"])
        stop_value = float(self._attr_config[attribute]["stop"])
        increase = float(self._attr_config[attribute]["increase"])
        sleep_time = float(self._attr_config[attribute]["sleep_time"])

        if return_proxy is True:
            attr = AttributeProxy(attribute)
            return (
                attr,
                readback,
                start_value,
                stop_value,
                increase,
                sleep_time,
            )
        else:
            return start_value, stop_value, increase, sleep_time

    @command(
        dtype_in=str,
        doc_in=(
            "Ramp attribute defined in config.yaml file. "
            "Input:\nattribute: Attribute name defined "
            "in config.yaml file."
        ),
    )
    def ramp_attribute(self, attribute):
        """Command to launch a ramp from YAML file.

        :param attribute: Name of the attribute to ramp inside YAML file.
        :type attribute: str
        """
        if attribute not in self._ramping_attrs:
            self._ramping_attrs.append(str(attribute))

            self.info_stream("Attribute: {}".format(attribute))

            (
                attr,
                readback,
                start_value,
                stop_value,
                inc,
                sleep_time,
            ) = self._config_my_ramp(attribute)

            self._check_attribute_limits(attr, start_value)
            self._check_attribute_limits(attr, stop_value)

            stop_event = Event()
            my_ramp = Thread(
                target=self._ramp_me,
                args=[
                    attr,
                    readback,
                    start_value,
                    stop_value,
                    stop_event,
                    inc,
                    sleep_time,
                ],
            )

            self._ramping_threads[str(attribute)] = stop_event
            my_ramp.start()

        else:
            Except.throw_exception(
                "ValueError", "Attribute is ramping now.", "LinacRamping"
            )

    @command(dtype_in="string")
    def stop_ramping(self, attribute):
        """Command to stop ramping.

        :param attribute: Attrbute that you want to stop ramp.
        :type attribute: str
        """
        if str(attribute) in self._ramping_threads:
            th = self._ramping_threads[str(attribute)]
            th.set()
        else:
            Except.throw_exception(
                "ValueError", "Attribute is not ramping.", "LinacRamp"
            )

    @command(
        dtype_in="string",
        doc_in=(
            "Ramp attribute with format: "
            "'attribute,readback,start,stop,delta_time,delta_value' where:"
            "\nattribute: tango attribute to ramp in 'a/b/c/d' format"
            "\nreadback: tango attribute to check if values are applied "
            "in 'a/b/c/d' format. If not needed, write None"
            "\nstart: Starting attribute value. Must be between max. "
            "and min. attribute config. value."
            "\nstop: Final attribute value. Must be between max. and min. "
            "attribute config. value."
            "\ndelta_time: Time to wait between steps."
            "\ndelta_value: Value to increase in every step."
        ),
    )
    def ramp_attribute_manual(self, config):
        """Command to launch a ramp.

        :param config: 'attribute,readback,start,stop,delta_time,delta_value'
        :type config: str
        """
        (
            attr,
            readback,
            start_value,
            stop_value,
            sleep_time,
            inc,
        ) = config.split(",")

        if attr not in self._ramping_attrs:
            attr_proxy = AttributeProxy(attr)

            self._ramping_attrs.append(str(attr))
            self.info_stream("Attribute: {}".format(attr))

            self._check_attribute_limits(attr_proxy, start_value)
            self._check_attribute_limits(attr_proxy, stop_value)

            stop_event = Event()
            my_ramp = Thread(
                target=self._ramp_me,
                args=[
                    attr_proxy,
                    readback,
                    start_value,
                    stop_value,
                    stop_event,
                    inc,
                    sleep_time,
                ],
            )

            my_ramp.start()
            self._ramping_threads[str(attr)] = stop_event

        else:
            Except.throw_exception(
                "ValueError", "Attribute is ramping now.", "LinacRamping"
            )

    def _send_mail(
        self, alarm_type, attr_proxy=None, attr_name=None, last_value_sent=None
    ):
        """Method to send mails with alarms.

        :param alarm_type: Type of alarm to send.
        :type alarm_type: str
        :param attr_proxy: Proxy object of ramping attribute, defaults to None
        :type attr_proxy: tango.AttributeProxy, optional
        :param attr_name: Name of the attribute, defaults to None
        :type attr_name: str, optional
        :param last_value_sent: Last value of the ramp, defaults to None
        :type last_value_sent: float, optional
        """
        server = "smtp.cells.es"
        send_from = "root@controls07.cells.es"
        send_to = ["emorales@cells.es", "dlanaia@cells.es", "rmunoz@cells.es"]
        subject = "LinacRamping problems"

        if attr_name is not None:
            attr_name = attr_name.lower()
            alarms_dict = self._attribute_alarms[attr_name]

        # Start to write message:
        msg = MIMEMultipart()
        msg["From"] = "Controls User <{}>".format(send_from)
        msg["Bcc"] = COMMASPACE.join(send_to)
        msg["Date"] = formatdate(localtime=True)
        msg["Subject"] = subject

        if alarm_type in ["SETPOINT"]:
            text = SETPOINT_ERROR
            text += "\tAttribute: {}\n".format(attr_name)
            text += "\tLast value sent:{}\n".format(last_value_sent)
            text += "\tActual value:{}\n\n".format(attr_proxy.read().value)
        elif alarm_type in ["KY_ON"]:
            text = KYON_ERROR
            text += "\tAttribute: {}\n".format(attr_name)
        elif alarm_type in ["ALARM"]:
            for item in alarms_dict:
                _val = self._myDict[item][1].attr_value
                _min = alarms_dict[item]["min_value"]
                _max = alarms_dict[item]["max_value"]
                if float(_val) <= float(_min) or float(_val) >= float(_max):
                    text = ALARM_ERROR
                    text += "\tAttribute: {}\n".format(item)
                    text += "\tActual value: {}\n".format(_val)
                    text += "\tAlarm min. value: {}\n".format(_min)
                    text += "\tAlarm max. value: {}\n\n".format(_max)
        elif alarm_type in ["RESET_OK"]:
            text = KY_RESET_OK
        elif alarm_type in ["RESET_NOK"]:
            text = KY_RESET_NOK
        else:
            text = (
                "LINAC RAMPING  - ERROR NOT RECOGNIZED.\n\n"
                "Please, contact emorales@cells.es"
            )

        text += "Kind Regards.\n"
        text += "Controls Group"

        msg.attach(MIMEText(text))

        smtp = smtplib.SMTP(server)
        smtp.sendmail(send_from, send_to, msg.as_string())
        smtp.close()

    def read_problems(self):
        return self._error


def main():
    server_run((LinacRamping,))


if __name__ == "__main__":
    main()
